  var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParser = require('body-parser')
app.use(bodyParser.json())


var requestJson = require('request-json');

var path = require('path');

var urlMlab = "https://api.mlab.com/api/1/databases/bdbanca2/collections/movimientos?apiKey=KqjyvWKHeJTPtoU--S8R1Ruiyocz-20L";

var urlMlab2 = "mongodb://admin123:admin123@ds127362.mlab.com:27362/bdbanca2";

var clienteMlab = requestJson.createClient(urlMlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);


app.get("/", function(req, res){

res.sendFile(path.join(__dirname, 'index.html'));

})

app.get("/clientes/:idCliente", function(req, res){
  res.send("Aqui tiene el cliente numero: " + req.params.idCliente);
});

app.post("/movimientos", function(req, res){

  var response = res;
  var MongoClient = require('mongodb').MongoClient;
  var url = urlMlab2;
  var clienteActual;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bdbanca2");
    var query = {"productos.cuentasTDC.plastico": req.body.plastico };
    dbo.collection("movimientos").find(query).toArray(function(err, result) {
      if (err) {
        throw err;
      }
      else {
        var resultadoConsulta = result;
        db.close();
        response.send(resultadoConsulta);

      }
    });
  });

});

app.post("/", function(req, res){
  res.send("He recibido su peticion satisfactoriamente");
})

app.get("/obtainDateNow", function(req, res){
  var dayNow = new Date();
  var day = dayNow.getDate();
  if (day < 10) {
    day = "0"+day;
  }
  var month = dayNow.getMonth() + 1;
  if (month < 10) {
    month = "0" + month;
  }

  var year = dayNow.getFullYear();

  var objectDate = {
    day : day + "/" + month + "/" + year
  }

  res.send(objectDate);
});


app.post("/saveMovement", function(req, res){
var response = res;

var movimientoEnviado =  req.body;
  var nuevoMovimiento = {
    idMovimiento : "",
    fechaMovimiento : movimientoEnviado.fechaMovimiento,
    descripcion : movimientoEnviado.descripcion,
    importe :  movimientoEnviado.importe,
    tipoMovimiento : movimientoEnviado.tipoMovimiento
  }


  var MongoClient = require('mongodb').MongoClient;
  var url = urlMlab2;
  var clienteActual;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bdbanca2");
    var query = {"productos.cuentasTDC.plastico": req.body.plastico };
    dbo.collection("movimientos").find(query).toArray(function(err, result) {
      if (err) {
        throw err;
      }
      else {
        clienteActual = result[0];
      clienteActual.productos.cuentasTDC.forEach(function(plastico){
        if (plastico.plastico == movimientoEnviado.plastico) {
          plastico.movimientos.push(nuevoMovimiento);
          dbo.collection("movimientos").updateOne(query, { $set : clienteActual }, { upsert: true } , function(err, res) {
          if (err) {
            throw err;
          }
          else{
            db.close();
            response.sendStatus(200);
          }
        });
        }
      });
      }
      db.close();
    });
  });

});

app.post("/iniciarSesion", function(req, res){

  var response = res;
  var MongoClient = require('mongodb').MongoClient;
  var url = urlMlab2;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bdbanca2");
    var query = {"usuario": req.body.usuario , "password": req.body.password };
    dbo.collection("Users").find(query).toArray(function(err, result) {
      if (err) {
        throw err;
      }
      else {
        var resultadoUsuario = result;
        db.close();
        response.send(resultadoUsuario);

      }
    });
  });

});

app.post("/guardarUsuario", function(req, res){

  var response = res;
  var MongoClient = require('mongodb').MongoClient;
  var url = urlMlab2;

  MongoClient.connect (url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bdbanca2");
    var usuario = {
      nombre : req.body.nombre,
      usuario : req.body.usuario,
      password :  req.body.password
    }
    dbo.collection("Users").insertOne(usuario, {w:1} , function(err, result) {
      if (err) {
        throw err;
      }
      else {
        var mensaje = "Registro Almacenado ok";
        db.close();
        response.send(mensaje);
      }
    });
  });
});
